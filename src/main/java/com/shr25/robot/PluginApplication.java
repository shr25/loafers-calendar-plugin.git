package com.shr25.robot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: 启动类
 * @author:: huobing
 * @date: 2022-9-4 18:11
 **/
@SpringBootApplication
public class PluginApplication {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(PluginApplication.class, args);
        Thread.currentThread().join();
    }
}
