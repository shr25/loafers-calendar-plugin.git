package com.shr25.robot.plugin;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.shr25.robot.common.RobotMsgType;
import com.shr25.robot.qq.model.QqMessage;
import com.shr25.robot.qq.plugins.RobotPlugin;
import com.shr25.robot.qq.util.MessageUtil;
import com.shr25.robot.utils.DateUtils;
import net.mamoe.mirai.message.data.Image;
import net.mamoe.mirai.message.data.MessageChainBuilder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @description: demo插件
 * @author:: huobing
 * @date: 2022-9-4 18:48
 **/
@Service
public class LoafersCalendarPlugin extends RobotPlugin {
    Map<String, Image> currDayImg = new HashMap<>();

    public LoafersCalendarPlugin() {
        super("摸鱼日历插件");
        addDescLn("摸鱼一时爽，一直摸鱼一直……");
        addCommand("摸鱼日历","获取今日摸鱼图片", qqMessage -> {
            sendImagesByUrl(qqMessage);
            return true;
        }, true);
        setSort(10);
    }

    private void sendImagesByUrl(QqMessage qqMessage) {
        String currDay = DateUtils.dateFormat(new Date(), "yyyy-MM-dd");
        Image image = currDayImg.get(currDay);
        if (image == null) {
            currDayImg.clear();
            HttpResponse response = HttpRequest.get("https://api.vvhan.com/api/moyu").setFollowRedirects(true).execute();
            image = MessageUtil.buildImageMessage(qqMessage.getContact(), response.bodyStream());
            currDayImg.put(currDay, image);
        }

        if (image != null) {
            qqMessage.putReplyMessage(image);
        }
    }
}
